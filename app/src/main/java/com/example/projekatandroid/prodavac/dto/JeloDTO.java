package com.example.projekatandroid.prodavac.dto;

import java.io.Serializable;

public class JeloDTO implements Serializable {
String naziv,opis;
Double cena;

    public JeloDTO() {
    }

    public JeloDTO(String naziv, String opis, Double cena) {
        this.naziv = naziv;
        this.opis = opis;
        this.cena = cena;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }
}

package com.example.projekatandroid.prodavac;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projekatandroid.R;
import com.example.projekatandroid.prodavac.dto.ProdavacDTO;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;


public class ProdavacInfoFragment extends Fragment {

    TextInputEditText ime,prezime,email,username,adresa,nazivFirme;
    private ProgressDialog progressDialog = null;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_prodavac_info, container,false);
        requireActivity().setTitle("Info");

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if(view != null) {
            nazivFirme = view.findViewById(R.id.txtNazivFirmeProdavac);
            email = view.findViewById(R.id.txtEmailProdavac);
            ime = view.findViewById(R.id.txtImeProdavac);
            prezime = view.findViewById(R.id.txtPrezimeProdavac);
            adresa = view.findViewById(R.id.txtAdresaProdavac);
            username = view.findViewById(R.id.txtUsernameProdavac);
            getUserInfo();
        }
    }

    private void getUserInfo() {
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
        progressDialog.show();
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("prodavac").child(userId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ProdavacDTO prodavacDTO = snapshot.getValue(ProdavacDTO.class);
                nazivFirme.setText(prodavacDTO.getNazivFirme());
                ime.setText(prodavacDTO.getIme());
                prezime.setText(prodavacDTO.getPrezime());
                email.setText(prodavacDTO.getEmail());
                adresa.setText(prodavacDTO.getAdresa());
                username.setText(prodavacDTO.getUsername());
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }


        });
    }
}
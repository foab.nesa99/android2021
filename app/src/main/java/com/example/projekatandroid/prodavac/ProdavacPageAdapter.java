package com.example.projekatandroid.prodavac;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ProdavacPageAdapter extends FragmentStateAdapter {
    public ProdavacPageAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            default:
                return new ProdavacInfoFragment();
            case 0:
                return new ProdavacMeniFragment();
            case 1:
                return new ProdavacAkcijaFragment();
            case 2:
                return new ProdavacOceneFragment();
            case 3:
                return new ProdavacPorudzbineFragment();
        }
    }
    @Override
    public int getItemCount() {
        return 5;
    }
}
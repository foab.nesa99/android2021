package com.example.projekatandroid.prodavac;

import android.os.Bundle;

import com.example.projekatandroid.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

public class ProdavacMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prodavac_main);

        ViewPager2 viewPagerProd = findViewById(R.id.viewPagerProd);
        viewPagerProd.setAdapter(new ProdavacPageAdapter(this));

        TabLayout tabLayout12 = findViewById(R.id.tabLayoutProd);
        TabLayoutMediator tabLayoutMediator12 = new TabLayoutMediator(tabLayout12, viewPagerProd ,new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {

                switch (position){
                    case 0:
                        tab.setText("Porudzbine");

                        break;
                    case 1:
                        tab.setText("Ocene");

                        break;
                    case 2:
                        tab.setText("Akcija");

                        break;
                    case 3:
                        tab.setText("MEni");

                        break;
                    case 4:
                        tab.setText("Info");

                        break;

                }

            }
        });
        tabLayoutMediator12.attach();
    }
}
package com.example.projekatandroid.prodavac.dto;

public class ProdavacDTO {
private String ime;
private String prezime;
private String email;
private String username;
private String adresa;
private String nazivFirme;

    public ProdavacDTO() {
        ime = "ime";
        prezime = "prez";
        username = "user";
        adresa = "adr";
        nazivFirme = "nazFir";
    }

    public ProdavacDTO(String ime, String prezime, String email, String username, String adresa, String nazivFirme) {
        this.ime = ime;
        this.prezime = prezime;
        this.email = email;
        this.username = username;
        this.adresa = adresa;
        this.nazivFirme = nazivFirme;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNazivFirme() {
        return nazivFirme;
    }

    public void setNazivFirme(String nazivFirme) {
        this.nazivFirme = nazivFirme;
    }
}

package com.example.projekatandroid.prodavac;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.projekatandroid.R;
import com.example.projekatandroid.prodavac.dto.ArtikalDTOAdd;
import com.example.projekatandroid.prodavac.dto.ArttikalDTOEdit;
import com.example.projekatandroid.prodavac.dto.JeloDTO;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class ArtikalBrisanjeIzmena extends AppCompatActivity {
    TextInputEditText Naziv,Cena,Opis;
    String naziv,opis,id;
    Double cena;
    MaterialButton izmenaBtn,obrisiBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_prodavac_izmena_brisanje);
        setTitle("Product details");
        Naziv = findViewById(R.id.txtNazivJelaIzmena);
        Opis = findViewById(R.id.txtOpisJelaIzmena);
        Cena = findViewById(R.id.txtCenaJelaIzmena);
        izmenaBtn = findViewById(R.id.btnIzmenaJela);
        obrisiBtn = findViewById(R.id.btnBrisanjeJela);
        String id1 = FirebaseDatabase.getInstance().getReference("prodavac").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("items").getKey();
        getArtikal();
        setArtikal();
        izmenaBtn.setOnClickListener(v -> {
            naziv = Naziv.getEditableText().toString().trim();
            opis = Opis.getEditableText().toString().trim();
            cena = Double.valueOf(Cena.getEditableText().toString().trim());
            if(naziv.isEmpty() || opis.isEmpty() || cena.isNaN() || cena == 0) {
                Toast.makeText(this, "Unos nije validan", Toast.LENGTH_SHORT).show();
            }
            else{
                String prodavacID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                JeloDTO newDish = new JeloDTO(naziv,opis,Double.valueOf(cena));
                FirebaseDatabase.getInstance().getReference("prodavac").child(prodavacID).child("items").child(id).setValue(newDish).addOnCompleteListener(task -> {
                    Toast.makeText(this, "Uspesno izvrsena izmena", Toast.LENGTH_SHORT).show();
                });
            }
        });

        obrisiBtn.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to delete this dish?");
            builder.setPositiveButton("YES", (dialog, which) -> {
                FirebaseDatabase.getInstance().getReference("prodavac").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("items").child(id).removeValue();
                AlertDialog.Builder food = new AlertDialog.Builder(this);
                food.setMessage("Your Dish has been Deleted");

            });
            builder.setNegativeButton("NO", (dialog, which) -> dialog.cancel());
            AlertDialog alert = builder.create();
            alert.show();
        });


    }

    private void getArtikal(){
        if(getIntent().hasExtra("cena") && getIntent().hasExtra("naziv") && getIntent().hasExtra("opis") && getIntent().hasExtra("id")) {
            id = getIntent().getStringExtra("id");
            naziv = getIntent().getStringExtra("name");
            opis = getIntent().getStringExtra("desc");
            cena = getIntent().getDoubleExtra("price", 0 );
        }
        else{
            Toast.makeText(this,"Nema podataka.",Toast.LENGTH_SHORT).show();
        }
    }

    private void setArtikal(){
        Naziv.setText(naziv);
        Opis.setText(opis);
        Cena.setText(Double.valueOf(cena).toString());
    }
}
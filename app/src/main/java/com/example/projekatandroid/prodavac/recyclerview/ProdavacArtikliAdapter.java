package com.example.projekatandroid.prodavac.recyclerview;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekatandroid.R;
import com.example.projekatandroid.prodavac.ArtikalBrisanjeIzmena;
import com.example.projekatandroid.prodavac.ProdavacMain;
import com.example.projekatandroid.prodavac.ProdavacMeniFragment;
import com.example.projekatandroid.prodavac.dto.ArtikalDTOAdd;
import com.example.projekatandroid.prodavac.dto.ArttikalDTOEdit;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ProdavacArtikliAdapter extends RecyclerView.Adapter<ProdavacArtikliAdapter.ViewHolder> {

    private Context mContext;
    private List<ArtikalDTOAdd> listaArtikala;

    public ProdavacArtikliAdapter(Context mContext, List<ArtikalDTOAdd> listaArtikala) {
        this.mContext = mContext;
        this.listaArtikala = listaArtikala;
    }

    @NonNull
    @Override
    public ProdavacArtikliAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.prodavac_jelo_fragment,parent,false);
        return new ProdavacArtikliAdapter.ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ProdavacArtikliAdapter.ViewHolder holder, int position) {
        final ArtikalDTOAdd artikalAdapter = listaArtikala.get(position);
        holder.Cena.setText(artikalAdapter.getCena().toString());
        holder.NazivJela.setText(artikalAdapter.getNaziv());
        holder.Opis.setText(artikalAdapter.getOpis());
        holder.Id.setText(artikalAdapter.getID());
        holder.kartica.setOnClickListener(v -> {
            Intent i = new Intent(mContext, ArtikalBrisanjeIzmena.class);
            ArtikalDTOAdd izmenaArt = listaArtikala.get(position);
            i.putExtra("id", izmenaArt.getID());
            i.putExtra("naziv", izmenaArt.getNaziv());
            i.putExtra("cena",izmenaArt.getCena());
            i.putExtra("opis",izmenaArt.getOpis());
            mContext.startActivity(i);
        });
    }

    @Override
    public int getItemCount() {
        return listaArtikala.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{


        MaterialTextView NazivJela,Cena,Opis,Id;
        CardView kartica;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Id = itemView.findViewById(R.id.txtIdArtikla);
            NazivJela = itemView.findViewById(R.id.txtNazivArtiklaMenu);
            kartica = itemView.findViewById(R.id.jednoJeloFrag);
            Cena = itemView.findViewById(R.id.txtCenaArtikla);
            Opis = itemView.findViewById(R.id.txtOpisArtikla);


        }
    }
}
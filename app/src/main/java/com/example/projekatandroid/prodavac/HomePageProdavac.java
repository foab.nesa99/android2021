package com.example.projekatandroid.prodavac;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.projekatandroid.MainActivity;
import com.example.projekatandroid.R;
import com.example.projekatandroid.prodavac.ProdavacPageAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class HomePageProdavac extends AppCompatActivity {


    @Override
    public void onBackPressed()
    {
        backBtn();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_prodavac);

        ViewPager2 viewPagerProd = findViewById(R.id.viewPagerProd);
        viewPagerProd.setAdapter(new ProdavacPageAdapter(this));

        TabLayout tabLayoutProd = findViewById(R.id.tabLayoutProd);
        TabLayoutMediator tabLayoutMediatorProd = new TabLayoutMediator(tabLayoutProd, viewPagerProd, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {

                switch (position){
                    case 0:
                        tab.setText("Meni");
                        tab.setIcon(R.drawable.pizza_24);
                        break;
                    case 1:
                        tab.setText("Akcija");
                        tab.setIcon(R.drawable.akcije_24);
                        break;
                    case 2:
                        tab.setText("Ocene");
                        tab.setIcon(R.drawable.ocene_24);
                        break;
                    case 3:
                        tab.setText("Porudzbine");
                        tab.setIcon(R.drawable.dostava_24);
                        break;
                    case 4:
                        tab.setText("Info");
                        tab.setIcon(R.drawable.info_24);
                        break;

                }

            }
        });
        tabLayoutMediatorProd.attach();


    }

    public void backBtn(){

        new AlertDialog.Builder(this)
                .setTitle("Log out")
                .setMessage("Da li ste sigurni da zelite da se izlogujete?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {



                        Intent j = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(j);
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton("NE", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();



    }



}
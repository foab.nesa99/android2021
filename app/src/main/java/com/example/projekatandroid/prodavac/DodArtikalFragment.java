package com.example.projekatandroid.prodavac;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.projekatandroid.R;
import com.example.projekatandroid.model.Artikal;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class DodArtikalFragment extends Fragment {

    Button dodajNovi;
    MaterialButton dodajArtikalBtn, gotovoBtn;
    TextInputEditText Naziv,Cena,Opis;
    String naziv,opis,cena;
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth FAuth;
    public DodArtikalFragment() {
        // Required empty public constructor
    }
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dod_artikal,container, false);
        getActivity().setTitle("Dodavanje artikla");
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        dodajNovi = view.findViewById(R.id.btnNoviArtikal);
        FAuth = FirebaseAuth.getInstance();
        String userId = FAuth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("prodavac").child(userId).child("items");
        if (view != null) {
            Naziv = view.findViewById(R.id.txtNazivArtikla);
            Opis = view.findViewById(R.id.txtOpis);
            dodajArtikalBtn = view.findViewById(R.id.btnDodajArtikal);
            Cena = view.findViewById(R.id.txtCena);
            gotovoBtn = view.findViewById(R.id.btnDodajArtikalGotov);

            gotovoBtn.setOnClickListener(v -> {

                ProdavacMeniFragment prodavacMeniFragment= new ProdavacMeniFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentDodaj, prodavacMeniFragment, "meniFragment")
                        .addToBackStack(null)
                        .commit();
                dodajNovi.setVisibility(View.VISIBLE);

            });

            dodajArtikalBtn.setOnClickListener(v -> {
                naziv = Naziv.getEditableText().toString().trim();
                opis = Opis.getEditableText().toString().trim();
                cena = Cena.getEditableText().toString().trim();

                    ProgressDialog mDialog = new ProgressDialog(getActivity());
                    mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    mDialog.setCancelable(false);
                    mDialog.setCanceledOnTouchOutside(false);
                    mDialog.setMessage("Artikal se dodaje, molim sacekajte");
                    mDialog.show();
                    Artikal noviArtikal = new Artikal(naziv,opis,Double.valueOf(cena));
                    String key = FirebaseDatabase.getInstance().getReference("prodavac").child(userId).child("items").push().getKey();
                    databaseReference.child(key).setValue(noviArtikal).addOnSuccessListener(aVoid -> {
                        mDialog.dismiss();
                        Toast.makeText(getContext(), "Artikal dodat.", Toast.LENGTH_SHORT).show();
                    });
            });
        }
                else{
                    Toast.makeText(getContext(), "Input error.", Toast.LENGTH_SHORT).show();


            }
        }
    }



package com.example.projekatandroid.prodavac.dto;

public class ArttikalDTOEdit {
    private String ID;
    String naziv,opis;
    Double cena;

    public ArttikalDTOEdit (String naziv, String opis, Double cena) {
        this.ID = ID;
        this.naziv = naziv;
        this.opis = opis;
        this.cena = cena;
    }

    public ArttikalDTOEdit() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }
}

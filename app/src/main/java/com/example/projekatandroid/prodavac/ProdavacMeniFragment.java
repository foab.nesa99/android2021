package com.example.projekatandroid.prodavac;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.projekatandroid.R;
import com.example.projekatandroid.adapter.ArtikalAdapter;
import com.example.projekatandroid.prodavac.dto.ArtikalDTOAdd;
import com.example.projekatandroid.prodavac.dto.ArttikalDTOEdit;
import com.example.projekatandroid.prodavac.recyclerview.ProdavacArtikliAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class ProdavacMeniFragment extends Fragment {
    RecyclerView recyclerView;
    private List<ArtikalDTOAdd> artikli = new ArrayList<>();
    String id;
    MaterialButton btnNoviArtikal,btnBrisiArtikal;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_prodavac_meni, container, false);
        getActivity().setTitle("Dodavanje artikla");


        recyclerView = v.findViewById(R.id.recycler_view_artikli_prodavac);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("prodavac").child(userId).child("items");
        ProgressDialog mDialog = new ProgressDialog(getContext());
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setMessage("Loading....");
        mDialog.show();
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
               fetchSalesmanItems();
                mDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }


        });
        return v;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();


        btnNoviArtikal = view.findViewById(R.id.btnNoviArtikal);

        btnNoviArtikal.setOnClickListener(v -> {

            DodArtikalFragment nextFrag= new DodArtikalFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.prodavacMeniFragment, nextFrag, "findThisFragment")
                    .addToBackStack(null)
                    .commit();

            btnNoviArtikal.setVisibility(View.INVISIBLE);
        });



    }

    private void fetchSalesmanItems() {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("prodavac").child(userId).child("items");


         reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                artikli.clear();
               for(DataSnapshot snapshot1 : snapshot.getChildren()) {
                    ArtikalDTOAdd jelo = snapshot1.getValue(ArtikalDTOAdd.class);
                    ArtikalDTOAdd editJelo = new ArtikalDTOAdd(snapshot1.getKey(), jelo.getNaziv(), jelo.getOpis(), jelo.getCena());
                   artikli.add(editJelo);
                }
                ProdavacArtikliAdapter art = new ProdavacArtikliAdapter(getContext(),artikli);
        recyclerView.setAdapter(art);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }



}
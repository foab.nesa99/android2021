package com.example.projekatandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.projekatandroid.prodavac.HomePageProdavac;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;


public class Registracija extends AppCompatActivity {
    TextInputEditText txtEmail;
    TextInputEditText txtPass, txtIme, txtPrezime, txtAdresa, txtUsername, txtNazivFirme;
    String email;
    String pass, ime, prezime, adresa, username, nazivFirme, uloga;
    DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registracija);
        Intent i = new Intent(this, HomePageKupac.class);
        Intent j = new Intent(this, RegDodProd.class);
        final int[] kor = {0};
        FirebaseAuth firebaseAuth;
        firebaseAuth = FirebaseAuth.getInstance();
        txtPass = findViewById(R.id.txtPassword);
        txtEmail = findViewById(R.id.txtEmailField);
        txtIme = findViewById(R.id.txtIme);
        txtPrezime = findViewById(R.id.txtPrezime);
        txtAdresa = findViewById(R.id.txtAdresa);
        txtNazivFirme = findViewById(R.id.txtNazFir);
        txtUsername = findViewById(R.id.txtUsername);

        final Boolean[] prodavac = new Boolean[1];
        RadioGroup rdg = findViewById(R.id.radioGroup);

        Button btnReg = findViewById(R.id.btnRegistrujSe);
        RadioButton checkedButton;

        rdg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                TextInputLayout txtNaziv = findViewById(R.id.txtNazivFirme);
                Boolean prodaje;
                View radioButton = rdg.findViewById(checkedId);
                int index = rdg.indexOfChild(radioButton);
                switch (index) {


                    case 0:
                        txtNaziv.setVisibility(View.GONE);
                        prodavac[0] = false;
                        break;
                    case 1:

                        prodavac[0] = true;
                        txtNaziv.setVisibility(View.VISIBLE);
                        break;

                }
            }
        });


        btnReg.setOnClickListener(v -> {
            email = txtEmail.getText().toString();
            pass = txtPass.getText().toString();

            if (rdg.getCheckedRadioButtonId() == -1) {
                Snackbar snackbar = Snackbar.make(v, "Molim, odaberite ulogu!", Snackbar.LENGTH_SHORT);
                snackbar.show();
            } else if (email.equals("")) {
                Snackbar snackbar = Snackbar.make(v, "Email prazan!", Snackbar.LENGTH_SHORT);
                snackbar.show();

            } else if (pass.equals("")) {
                Snackbar snackbar = Snackbar.make(v, "Pass prazan!", Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
             else {
                firebaseAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {

                        ime = txtIme.getEditableText().toString().trim();
                        prezime = txtPrezime.getEditableText().toString().trim();
                        adresa = txtAdresa.getEditableText().toString().trim();
                        pass = txtAdresa.getEditableText().toString().trim();
                        username = txtUsername.getEditableText().toString().trim();
                        nazivFirme = txtNazivFirme.getEditableText().toString().trim();
                        email = txtEmail.getEditableText().toString().trim();

                        if(validacija()){
                        String user = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        databaseReference = FirebaseDatabase.getInstance().getReference("korisnici").child(user);
                        final HashMap<String, String> hashMap = new HashMap<>();

                        hashMap.put("Aktivan", "true");

                            HashMap<String, Object> mapa = new HashMap<>();
                            mapa.put("Ime", ime);
                            mapa.put("Prezime", prezime);
                            mapa.put("Adresa", adresa);
                            mapa.put("username", username);
                            mapa.put("email", email);


                            if (prodavac[0] == false) {

                                hashMap.put("Uloga", "kupac");
                               // databaseReference = FirebaseDatabase.getInstance().getReference("user");
                                databaseReference.setValue(hashMap);
                                firebaseDatabase.getInstance().getReference("kupac").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .setValue(mapa).addOnCompleteListener(a -> {

                                    firebaseAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(b -> {
                                        if (b.isSuccessful()) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                                            builder.setMessage("Uspesna registracija");
                                            builder.setCancelable(false);
                                            Intent i1 = new Intent(this, HomePageKupac.class);
                                            startActivity(i1);
                                            AlertDialog alert = builder.create();
                                            alert.show();
                                            Snackbar snackbar = Snackbar.make(v, "Uspesno ste kreirali nalog!", Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        } else {
                                            Snackbar snackbar1 = Snackbar.make(v, "Nesto ne valja!", Snackbar.LENGTH_LONG);
                                            snackbar1.show();
                                        }
                                    });
                                });


                            } else if (prodavac[0] == true) {
                                if (nazivFirme.equals("")) {
                                    Snackbar snackbartest = Snackbar.make(v, "Unesite naziv vase firme!", Snackbar.LENGTH_LONG);
                                    snackbartest.show();
                                } else {
                                    hashMap.put("Uloga", "prodavac");
                                    databaseReference.setValue(hashMap);
                                   // databaseReference = FirebaseDatabase.getInstance().getReference("user");
                                    mapa.put("nazivFirme", nazivFirme);
                                    mapa.put("ocna", 0);
                                    long millis = System.currentTimeMillis();
                                    Date date = new Date(millis);
                                    mapa.put("registrationDate", date.toString());
                                    FirebaseDatabase.getInstance().getReference("prodavac").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                            .setValue(mapa).addOnCompleteListener(a -> {
                                        Intent i2 = new Intent(Registracija.this, HomePageProdavac.class);
                                        startActivity(i2);
                                        Snackbar snackbar = Snackbar.make(v, "Uspesno ste kreirali nalog!", Snackbar.LENGTH_LONG);
                                        snackbar.show();

                                    });


                                }

                            }



                    }

                    }
                });


            }

        });

    }
public boolean validacija(){

        boolean validan = false;
        View v = new View(Registracija.this);
    if(ime.equals("")){

        txtIme.setError("Morate uneti ime");
        validan = false;
    }
    else if(prezime.equals("")){
        txtPrezime.setError("Morate uneti prezime");
        validan = false;
    }

    else if(adresa.equals("")){
        txtAdresa.setError("Morate uneti email");
        validan = false;
    }

    else if(username.equals("")){
        txtAdresa.setError("Morate uneti username");
        validan = false;

    }
   /* else if(pass.length()< 6){
        txtAdresa.setError("Lozinka mora biti duza od 6 karaktera");
        validan = false;

    }*/
     else{
         validan = true;

    }

     return validan;
}

}














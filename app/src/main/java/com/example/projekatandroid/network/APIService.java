package com.example.projekatandroid.network;

import com.example.projekatandroid.model.Artikal;
import com.example.projekatandroid.model.Prodavac;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIService {

    @GET("artikals")
    Call<List<Artikal>> vratiArtikal();

    @GET("prodavci")
    Call<List<Prodavac>> vratiProdavce();

}

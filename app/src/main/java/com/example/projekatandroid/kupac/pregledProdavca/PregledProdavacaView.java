package com.example.projekatandroid.kupac.pregledProdavca;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekatandroid.R;
import com.example.projekatandroid.kupac.ArtikliProdavcaActivity;
import com.example.projekatandroid.kupac.dto.ProdavacModel;
import com.example.projekatandroid.kupac.komentari.KomentariAdapter;

import java.util.List;

public class PregledProdavacaView extends RecyclerView.Adapter<PregledProdavacaView.ViewHolder> {

    private final Context mContext;
    private final List<ProdavacModel> listaProdavaca;

    public PregledProdavacaView(Context mContext, List<ProdavacModel> listaProdavaca) {
        this.mContext = mContext;
        this.listaProdavaca = listaProdavaca;
    }


    @NonNull
    @Override
    public PregledProdavacaView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fragment_prodavac_icon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PregledProdavacaView.ViewHolder holder, int position) {
        final ProdavacModel prodavacModel = listaProdavaca.get(position);
        holder.NazivFirme.setText(prodavacModel.getNazivFirme());
        holder.Ocena.setText("5");
       /* holder.komentari.setOnClickListener(v -> {
            Intent i = new Intent(mContext, KomentariAdapter.class);
            Bundle args = new Bundle();
            args.putSerializable("comments", );
            i.putExtra("bundle", args);
            mContext.startActivity(i);
        }); */
        holder.kartica.setOnClickListener(v -> {
            Intent i = new Intent(mContext, ArtikliProdavcaActivity.class);
            Bundle args = new Bundle();
            args.putSerializable("items", prodavacModel.getProizvodi());
            i.putExtra("prodavacID", prodavacModel.getId());
            i.putExtra("bundle", args);
            mContext.startActivity(i);
        });
    }

    @Override
    public int getItemCount() {
        return listaProdavaca.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView kartica;
        TextView NazivFirme, Ocena;
        Button komentari;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            NazivFirme = itemView.findViewById(R.id.txtNazivProdavcaFirma);
            Ocena = itemView.findViewById(R.id.txtOcenaProdavca);
            komentari = itemView.findViewById(R.id.prikazKomentara);
            kartica = itemView.findViewById(R.id.prodavacFrag);
        }
    }

}

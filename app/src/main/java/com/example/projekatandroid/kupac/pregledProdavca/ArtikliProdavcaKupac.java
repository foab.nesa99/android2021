package com.example.projekatandroid.kupac.pregledProdavca;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekatandroid.R;
import com.example.projekatandroid.kupac.dto.NarudzbinaModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ArtikliProdavcaKupac extends RecyclerView.Adapter<ArtikliProdavcaKupac.ViewHolder> {
private final Context mContext;
private final List<NarudzbinaModel> adapterNarudzbinaLista;
private final ArrayList<NarudzbinaModel> listaOdabranihArtikala = new ArrayList<>();
private int brojac;

    public ArtikliProdavcaKupac(Context mContext, List<NarudzbinaModel> adapterNarudzbinaLista) {
        this.mContext = mContext;
        this.adapterNarudzbinaLista = adapterNarudzbinaLista;
    }


    @NonNull
    @Override
        public ArtikliProdavcaKupac.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.prodavac_jelo_fragment,parent,false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull ArtikliProdavcaKupac.ViewHolder holder, int position) {



final NarudzbinaModel novaNarudzbina = adapterNarudzbinaLista.get(position);
        holder.NazivJela.setText(novaNarudzbina.getJelo().getNaziv());
        holder.Cena.setText(String.format("Cena %s" ,novaNarudzbina.getJelo().getCena()));
        holder.KolicinaBtn.setOnClickListener(v -> {
            brojac++;
            holder.Kolicina.setText(String.valueOf(brojac));

        });

        }

public ArrayList<NarudzbinaModel> getNaruceni() {
        return listaOdabranihArtikala;
        }

@Override
public int getItemCount() {
        return adapterNarudzbinaLista.size();
        }

public static class ViewHolder extends RecyclerView.ViewHolder{

    ImageView imageView;
    MaterialTextView NazivJela,Cena,Opis,Id,Kolicina;
    CardView kartica;
    MaterialButton poruciBtn;
    Button KolicinaBtn;
    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.slikaMenu);
        NazivJela = itemView.findViewById(R.id.txtNazivArtiklaKupac);
        Cena = itemView.findViewById(R.id.txtCenaArtiklaKupac);
        poruciBtn = itemView.findViewById(R.id.btnuKorpu);
        Kolicina = itemView.findViewById(R.id.txtKolicina);
        KolicinaBtn = itemView.findViewById(R.id.btnDodajPlusJedan);

    }
}

    }


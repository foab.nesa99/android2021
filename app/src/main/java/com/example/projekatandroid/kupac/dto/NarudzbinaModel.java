package com.example.projekatandroid.kupac.dto;

import com.example.projekatandroid.prodavac.dto.JeloDTO;

import java.io.Serializable;
import java.util.ArrayList;

public class NarudzbinaModel implements Serializable {

    private String id;
    private JeloDTO jelo;

    public NarudzbinaModel() {
        kolicina = 0;
    }

    public NarudzbinaModel(String id, JeloDTO jelo, int kolicina) {
        this.id = id;
        this.jelo = jelo;
        this.kolicina = kolicina;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JeloDTO getJelo() {
        return jelo;
    }

    public void setJelo(JeloDTO jelo) {
        this.jelo = jelo;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    private int kolicina;
}

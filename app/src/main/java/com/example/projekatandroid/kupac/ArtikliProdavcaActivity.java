package com.example.projekatandroid.kupac;

import android.content.Intent;
import android.os.Bundle;

import com.example.projekatandroid.kupac.dto.NarudzbinaModel;
import com.example.projekatandroid.kupac.pregledProdavca.ArtikliProdavcaKupac;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.projekatandroid.R;

import java.util.ArrayList;

public class ArtikliProdavcaActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<NarudzbinaModel> artikliProdavca;
    Button uKorpu;
    ArrayList<NarudzbinaModel> odabraniArtikliList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_artikli_prodavca_kupac);
        setTitle("Companies items");
        recyclerView = findViewById(R.id.recycler_views_artikli_kupac);
        artikliProdavca = getItems();
        String salesmanID = getIntent().getStringExtra("prodavacID");
        if(artikliProdavca != null){
            ArtikliProdavcaKupac s = new ArtikliProdavcaKupac(this,artikliProdavca);
            recyclerView.setAdapter(s);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            odabraniArtikliList = s.getNaruceni();
        }

        uKorpu = findViewById(R.id.btnuKorpu);
        /*uKorpu.setOnClickListener(v -> {
            if(!odabraniArtikliList.isEmpty()) {
                Intent i = new Intent(this, CustomerShoppingCartActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("items", odabraniArtikliList);
                i.putExtra("bundle", args);
                i.putExtra("salesmanID", salesmanID);
                startActivity(i);
            }
            else{
                Toast.makeText(this,"Please select an item.", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private ArrayList<NarudzbinaModel> getItems() {
        Bundle b = getIntent().getBundleExtra("bundle");
        artikliProdavca = (ArrayList<NarudzbinaModel>) b.getSerializable("items");
        return artikliProdavca;
    }
}
package com.example.projekatandroid.kupac;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.projekatandroid.BigPizzaActivity;
import com.example.projekatandroid.R;
import com.example.projekatandroid.kupac.dto.NarudzbinaModel;
import com.example.projekatandroid.kupac.dto.ProdavacModel;
import com.example.projekatandroid.kupac.pregledProdavca.PregledProdavacaView;
import com.example.projekatandroid.prodavac.dto.JeloDTO;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class KupacNaruciFragment extends Fragment{


    RecyclerView recyclerView;
    ArrayList<ProdavacModel> listaProdavaca = new ArrayList<>();



    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_kupac_naruci, null);
        getActivity().setTitle("Companies");
        recyclerView = v.findViewById(R.id.recycler_view_lista_prodavaca);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getData();
        return v;
    }

    private void getData() {

        FirebaseDatabase.getInstance().getReference("prodavac").addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                listaProdavaca.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()) {
                    ProdavacModel prodavacModel = new ProdavacModel();
                    prodavacModel.setId(snapshot1.getKey());
                    prodavacModel.setNazivFirme(snapshot1.child("nazivFirme").getValue(String.class));
                    prodavacModel.setOcena(snapshot1.child("ocena").getValue(Double.class));
                    ArrayList<NarudzbinaModel> listaJela = new ArrayList<>();
                    for(DataSnapshot snapshot2 : snapshot1.child("items").getChildren()) {
                        NarudzbinaModel d1 = new NarudzbinaModel();
                        d1.setId(snapshot2.getKey());
                        JeloDTO jelo = new JeloDTO();
                        jelo.setNaziv(snapshot2.child("naziv").getValue(String.class));
                        jelo.setOpis(snapshot2.child("opis").getValue(String.class));
                        jelo.setCena(snapshot2.child("cena").getValue(Double.class));
                        d1.setJelo(jelo);
                        d1.setKolicina(0);
                        listaJela.add(d1);
                    }

                    prodavacModel.setProizvodi(listaJela);

                    listaProdavaca.add(prodavacModel);
                    PregledProdavacaView shop = new PregledProdavacaView(getContext(), listaProdavaca);
                    recyclerView.setAdapter(shop);

                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


    }
        // or  (ImageView) view.findViewById(R.id.foo);
}
package com.example.projekatandroid.kupac.dto;

import java.util.ArrayList;

public class ProdavacModel {

    private String id;
    private String nazivFirme;
    private ArrayList<NarudzbinaModel> proizvodi;
    private Double ocena = 0.00;

    public ProdavacModel() {
    }

    public ProdavacModel(String id, String nazivFirme, ArrayList<NarudzbinaModel> proizvodi, Double ocena) {
        this.id = id;
        this.nazivFirme = nazivFirme;
        this.proizvodi = proizvodi;
        this.ocena = ocena;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNazivFirme() {
        return nazivFirme;
    }

    public void setNazivFirme(String nazivFirme) {
        this.nazivFirme = nazivFirme;
    }

    public ArrayList<NarudzbinaModel> getProizvodi() {
        return proizvodi;
    }

    public void setProizvodi(ArrayList<NarudzbinaModel> proizvodi) {
        this.proizvodi = proizvodi;
    }

    public Double getOcena() {
        return ocena;
    }

    public void setOcena(Double ocena) {
        this.ocena = ocena;
    }

    public ArrayList<Komentari> getKomentaris() {
        return komentaris;
    }

    public void setKomentaris(ArrayList<Komentari> komentaris) {
        this.komentaris = komentaris;
    }

    private ArrayList<Komentari> komentaris;


}

package com.example.projekatandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.projekatandroid.R;
import com.example.projekatandroid.model.Prodavac;
import com.example.projekatandroid.model.Prodavac;

import java.util.List;

public class ProdavacAdapter extends RecyclerView.Adapter<ProdavacAdapter.ProdavacViewHolder> {
    private Context context;
    private List<Prodavac> listaProdavaca;

    public ProdavacAdapter(Context context, List<Prodavac> listaProdavaca) {
        this.context = context;
        this.listaProdavaca = listaProdavaca;
    }

    public void setListaProdavaca(List<Prodavac> listaProdavaca){
        this.listaProdavaca = listaProdavaca;
        notifyDataSetChanged();


    }

    @NonNull
    @Override
    public ProdavacAdapter.ProdavacViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_red, parent , false);
        return new ProdavacAdapter.ProdavacViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull ProdavacAdapter.ProdavacViewHolder holder, int position) {
        holder.naslov.setText(this.listaProdavaca.get(position).getNaziv());


    }

    @Override
    public int getItemCount() {
        if(this.listaProdavaca != null){
            return this.listaProdavaca.size();

        }
        return 0;
    }

    public class ProdavacViewHolder extends RecyclerView.ViewHolder {
        TextView naslov;
        ImageView slika;


        public ProdavacViewHolder(View itemView){
            super(itemView);
            naslov = itemView.findViewById(R.id.naslov);
            slika = itemView.findViewById(R.id.imageView);


        }

    }


}
package com.example.projekatandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.projekatandroid.R;
import com.example.projekatandroid.model.Artikal;

import java.util.List;

public class ArtikalAdapter extends RecyclerView.Adapter<ArtikalAdapter.ArtikalViewHolder> {
    private Context context;
    private List<Artikal> listaArtikala;

    public ArtikalAdapter(Context context, List<Artikal> listaArtikala) {
        this.context = context;
        this.listaArtikala = listaArtikala;
    }

    public void setListaArtikala(List<Artikal> listaArtikala){
        this.listaArtikala = listaArtikala;
        notifyDataSetChanged();


    }

    @NonNull
    @Override
    public ArtikalAdapter.ArtikalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_red, parent , false);
        return new ArtikalViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull ArtikalAdapter.ArtikalViewHolder holder, int position) {
        holder.naslov.setText(this.listaArtikala.get(position).getNaziv());
        Glide.with(context).load(this.listaArtikala.get(position).getPutanjaSlike()).apply(RequestOptions.centerCropTransform()).into(holder.slika);

    }

    @Override
    public int getItemCount() {
        if(this.listaArtikala != null){
            return this.listaArtikala.size();

        }
        return 0;
    }

    public class ArtikalViewHolder extends RecyclerView.ViewHolder {
TextView naslov;
ImageView slika;


        public ArtikalViewHolder(View itemView){
            super(itemView);
            naslov = itemView.findViewById(R.id.naslov);
            slika = itemView.findViewById(R.id.imageView);


        }

    }


}

package com.example.projekatandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.projekatandroid.prodavac.HomePageProdavac;

public class LoginProdavacAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_prodavac);
        Button btnLoginMainProdavac = findViewById(R.id.btnLoginProdavac);
        Button btnRegisterProdavac = findViewById(R.id.btnRegistracijaProdavac);




        btnLoginMainProdavac.setOnClickListener(v -> {
            Intent j = new Intent(this, HomePageProdavac.class);
            startActivity(j);

        });
        btnRegisterProdavac.setOnClickListener(v -> {
            Intent i = new Intent(this,Registracija.class);
            startActivity(i);

        });

    }
}
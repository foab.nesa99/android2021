package com.example.projekatandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.projekatandroid.adapter.FirebaseCallback;
import com.example.projekatandroid.prodavac.HomePageProdavac;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginKupacAct extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    TextInputEditText email, pass;
    Boolean prodavac,kupac,admin;
    String role;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DatabaseReference reference;
        setContentView(R.layout.activity_login_kupac);
        email = findViewById(R.id.txtEmailLogin);
        pass = findViewById(R.id.txtPasswordLogin);
        firebaseAuth = FirebaseAuth.getInstance();
        String userID;
        Button btnLoginMainKupac = findViewById(R.id.btnLoginMainKupac);
        Button btnRegisterKupac = findViewById(R.id.btnRegistracijaKupac);
        btnLoginMainKupac.setOnClickListener(v -> {
            String txtEmail = email.getEditableText().toString().trim();
            String txtPass = pass.getEditableText().toString().trim();
            if (txtEmail.isEmpty() || txtPass.isEmpty()) {
                Toast.makeText(this, "Unesite ime i lozinku.", Toast.LENGTH_SHORT).show();
            } else {
                firebaseAuth.signInWithEmailAndPassword(txtEmail, txtPass).addOnCompleteListener(task -> {

                    if (task.isSuccessful()) {

                      //  userID = reference.FirebaseDatabase.getInstance().getReference("user").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Role");

                        readFirebaseName(name -> {
                            if (name.equals("kupac")) {

                                Toast.makeText(this, "Login successful.", Toast.LENGTH_SHORT).show();
                                Intent kupacLogin = new Intent(LoginKupacAct.this, HomePageKupac.class);
                                startActivity(kupacLogin);
                            } else {

                                Toast.makeText(this, "Login failed.", Toast.LENGTH_SHORT).show();
                            }
                            if(name.equals("prodavac")){
                                Toast.makeText(this, "Login successful.", Toast.LENGTH_SHORT).show();
                                Intent prodavacLogin = new Intent(LoginKupacAct.this, HomePageProdavac.class);
                                startActivity(prodavacLogin);

                            }
                        });
                    } else {

                        Toast.makeText(this, "Login failed.", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });
    }


    public void readFirebaseName(FirebaseCallback callback) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("korisnici").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Uloga");
        reference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String name = task.getResult().getValue(String.class);
                callback.onResponse(name);
            } else {
                Log.d("TAG", task.getException().getMessage());
            }
        });
    }
}
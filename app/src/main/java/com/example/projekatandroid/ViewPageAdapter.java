package com.example.projekatandroid;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.projekatandroid.kupac.KupacDostavaFragment;
import com.example.projekatandroid.kupac.KupacInfoFragment;
import com.example.projekatandroid.kupac.KupacNaruciFragment;

public class ViewPageAdapter extends FragmentStateAdapter {
    public ViewPageAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
            switch(position){
                default:
                    return new KupacInfoFragment();
                case 0:
                    return new KupacNaruciFragment();
                case 1:
                    return new KupacDostavaFragment();



            }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}

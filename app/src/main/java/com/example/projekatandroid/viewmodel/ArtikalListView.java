package com.example.projekatandroid.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.projekatandroid.model.Artikal;
import com.example.projekatandroid.network.APIService;
import com.example.projekatandroid.network.RetrofitInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArtikalListView extends ViewModel {


    private MutableLiveData<List<Artikal>> artikliLista;
    public ArtikalListView(){
        artikliLista = new MutableLiveData<>();


    }
    public MutableLiveData<List<Artikal>> vratiArtikliLista(){
        return artikliLista;

    }



    public void makeApiCall(){
        APIService apiService = RetrofitInstance.getRetrofitClient().create(APIService.class);
        Call<List<Artikal>> call = apiService.vratiArtikal();
        call.enqueue(new Callback<List<Artikal>>() {
            @Override
            public void onResponse(Call<List<Artikal>> call, Response<List<Artikal>> response) {
                artikliLista.postValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Artikal>> call, Throwable throwable) {
                artikliLista.postValue(null);
            }
        });

    }


}

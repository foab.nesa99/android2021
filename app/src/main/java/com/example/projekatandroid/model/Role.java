package com.example.projekatandroid.model;

public enum Role {
    ROLE_BUYER,
    ROLE_SELLER,
    ROLE_ADMIN

}

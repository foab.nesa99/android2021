package com.example.projekatandroid.model;

import java.util.Date;

public class Prodavac extends Korisnik{

    public Prodavac(Integer id, String username, String password, String ime, String prezime, boolean blokiran, Role role, Date poslujeOd, String email, String adresa, String naziv) {
        super(id, username, password, ime, prezime, blokiran, role);
        this.poslujeOd = poslujeOd;
        this.email = email;
        this.adresa = adresa;
        this.naziv = naziv;
    }

    private Date poslujeOd;

    private String email;

    private String adresa;

    private String naziv;

    public Date getPoslujeOd() {
        return poslujeOd;
    }

    public void setPoslujeOd(Date poslujeOd) {
        this.poslujeOd = poslujeOd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }


}

package com.example.projekatandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;

import com.example.projekatandroid.prodavac.HomePageProdavac;
import com.google.android.material.snackbar.Snackbar;

public class RegDodProd extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_dod_prod);

        Button btnRegisterProdavac = findViewById(R.id.btnRegistrujSeProdavac);
        btnRegisterProdavac.setOnClickListener(v -> {

        Intent i = new Intent(this, HomePageProdavac.class);
        Snackbar snackbar = Snackbar.make(v, "Cestitam,uspesno ste se registrovali!", Snackbar.LENGTH_LONG);
        snackbar.show();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


                    startActivity(i);


            }
        }, 2000);

        });

    }
}
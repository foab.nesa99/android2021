package com.example.projekatandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.projekatandroid.adapter.ArtikalAdapter;
import com.example.projekatandroid.model.Artikal;
import com.example.projekatandroid.viewmodel.ArtikalListView;

import java.util.List;

public class TestAct extends AppCompatActivity {

    private List<Artikal> artikalList;
    private ArtikalAdapter artikalAdapter;
    private ArtikalListView artikalListView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);


        TextView noResultText = findViewById(R.id.nemaTeksta);
        RecyclerView recyclerView = findViewById(R.id.recyclerArtikal);
        LinearLayoutManager layoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(layoutManager);
         artikalAdapter = new ArtikalAdapter(this,artikalList);
        recyclerView.setAdapter(artikalAdapter);


       artikalListView = ViewModelProviders.of(this).get(ArtikalListView.class);
        artikalListView.vratiArtikliLista().observe(this, new Observer<List<Artikal>>() {
            @Override
            public void onChanged(List<Artikal> artikals) {

                if(artikals != null){
                    artikalList = artikals;
                    artikalAdapter.setListaArtikala(artikals);
                }
                else{
                    noResultText.setVisibility(View.VISIBLE);


                }
            }
        });
    artikalListView.makeApiCall();
    }


}
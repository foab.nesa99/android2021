package com.example.projekatandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class HomePageKupac extends AppCompatActivity {


    @Override
    public void onBackPressed()
    {
        backBtn();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_kupac);
        ViewPager2 viewPager2 = findViewById(R.id.viewPager);
        viewPager2.setAdapter(new ViewPageAdapter(this));

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {

                switch (position){
                    case 0:
                        tab.setText("Narucite");
                        tab.setIcon(R.drawable.pizza_24);
                        break;
                    case 1:
                        tab.setText("Narudzbe");
                        tab.setIcon(R.drawable.dostava_24);
                        break;
                    case 2:
                        tab.setText("Korisnik");
                        tab.setIcon(R.drawable.info_24);
                        break;

                }

            }
        });
       tabLayoutMediator.attach();

    }

    public void backBtn(){

        new AlertDialog.Builder(this)
                .setTitle("Log out")
                .setMessage("Da li ste sigurni da zelite da se izlogujete?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {



                        Intent j = new Intent(getBaseContext(),MainActivity.class);
                        startActivity(j);
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton("NE", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();



    }


}